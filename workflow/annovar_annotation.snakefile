import re
from snakemake.utils import R

workdir: "/home/jchung/projects/wgs/"
variantFiles = "wgs.k1_only.maf1.deleterious.variant.txt wgs.k2_only.maf1.deleterious.variant.txt wgs.maf1.deleterious.variant.txt".split()
annovar_dir = '/home/jchung/programs/annovar/annovar2013Aug23/'
vcfFiles = "wgs_phased_indel.vcf wgs_phased_snp.vcf".split()
vcftools = "/apps1/vcftools/vcftools_0.1.11/cpp/vcftools"
vcftools_vcfisec = "/apps1/vcftools/vcftools_0.1.11/perl/vcf-isec"
# sampleid = "BM1452.001"
sampleid = "BM1452.001 BM1452.100 BM1452.200 BM1453.001 BM1453.100 BM1453.200".split()
probandid = "BM1452.001 BM1453.001".split()
var_type = "snp indel".split()
# var_type = "indel"

def remove_superdup(input, output):
    in_file = open(input, "r")
    out_file = open(output, "w")

    for line in in_file:
        if line.startswith("Chr\tStart"):
            split_line = line.split("\t")
            superdup_index = split_line.index("genomicSuperDups")
            out_file.write(line)
        elif line.startswith("#"):
            out_file.write(line)
        else:
            split_line = line.split("\t")
            if split_line[superdup_index] == "":
                out_file.write(line)
    in_file.close()
    out_file.close()

def extract_conserved_elements(input, output, conservation_column, score_cutoff):
    in_file = open(input, "r")
    out_file = open(output, "w")
    score_index = None
    
    for line in in_file:
        if line.startswith("Chr\tStart"):
            split_line = line.split("\t")
            score_index = split_line.index(conservation_column)
            out_file.write(line)
        elif line.startswith("#"):
            out_file.write(line)
        else:
            split_line = line.split("\t")
            score_column = split_line[score_index]
            
            if not score_column == "":
                score = score_column.split(";")[1]
                score = score.split("=")[2]
                if int(score) >= int(score_cutoff):
                    out_file.write(line)
    in_file.close()
    out_file.close()

def remove_previously_reported(input, output, dbsnp_database = "snp138"):
    in_file = open(input, "r")
    out_file = open(output, "w")
    
    for line in in_file:
        split_line = line.split("\t")
        
        if line.startswith("Chr\tStart"):
            esp_index = split_line.index("esp6500si_all")
            thousand_genomes_index = split_line.index("1000g2012apr_all")
            snp138_index = split_line.index(dbsnp_database)
            out_file.write(line)
        elif line.startswith("#"):
            out_file.write(line)
        elif not line.startswith("#"):
            # esp_annot = split_line[esp_index] == ""
            # thousand_genomes_annot = split_line[thousand_genomes_index] == ""
            snp138_annot = split_line[snp138_index] == ""
            
            # if all([esp_annot, thousand_genomes_annot, snp138_annot]):
            if snp138_annot:
                out_file.write(line)
    in_file.close()
    out_file.close()

def extract_genic_variants(input, output):
    in_file = open(input, "r")
    out_file = open(output, "w")
    
    re_pattern = re.compile("\sexonic\s|\ssplicing\s|\sUTR5\s|\sURT3\s|\sintronic\s")
    for line in in_file:
        if line.startswith("Chr\tStart"):
            out_file.write(line)
        elif line.startswith("#"):
            out_file.write(line)
        elif not line.startswith("#"):
            if re_pattern.search(line):
                out_file.write(line)
    
    in_file.close()
    out_file.close()

def extract_deleterious_variants(input, output, deleterious_score_cutoff):
    deleterious_score_cutoff = int(deleterious_score_cutoff)
    in_file = open(input, "r")
    out_file = open(output, "w")
    
    re_frameshift = re.compile("\sframeshift insertion\s|\sframeshift deletion\s|\sframeshift block substitution\s")
    re_stopalter = re.compile("\sstopgain\s|\sstoploss\s")
    re_nonsyn = re.compile("nonsynonymous")
    re_splice = re.compile("splicing")
    
    # Parse nonsynonymous SNVs
    for line in in_file:
        split_line = line.split("\t")
        
        if line.startswith("Chr\tStart"):
            sift_pred_index = split_line.index("LJB23_SIFT_pred")
            polyphen2_hdiv_pred_index = split_line.index("LJB23_Polyphen2_HDIV_pred")
            lrt_pred_index = split_line.index("LJB23_LRT_pred")
            mutationtaster_pred_index = split_line.index("LJB23_MutationTaster_pred")
            out_file.write(line)
        elif line.startswith("#"):
            out_file.write(line)
        elif re_nonsyn.search(line):
            # Test for deleteriousness
            sift = split_line[sift_pred_index] == "D"
            polyphen2 = (split_line[polyphen2_hdiv_pred_index] == "D") or (split_line[polyphen2_hdiv_pred_index] == "P")
            lrt = split_line[lrt_pred_index] == "D"
            mutationtaster = (split_line[mutationtaster_pred_index] == "A") or (split_line[mutationtaster_pred_index] == "D")
            
            deleterious_score = sum([sift, polyphen2, lrt, mutationtaster])
            if (deleterious_score >= deleterious_score_cutoff):
                out_file.write(line)
        elif re_frameshift.search(line):
            out_file.write(line)
        elif re_stopalter.search(line):
            out_file.write(line)
        elif re_splice.search(line):
            out_file.write(line)
            
    in_file.close()
    out_file.close()

def extract_rare_variants(input, output, maf_cutoff):
    in_file = open(input, "r")
    out_file = open(output, "w")
    
    for line in in_file:
        split_line = line.split("\t")
        
        if line.startswith("Chr\tStart"):
            allelefreq_index = split_line.index("1000g2012apr_all")
            out_file.write(line)
        elif line.startswith("#"):
            out_file.write(line)
        elif not line.startswith("#"):
            if split_line[allelefreq_index] == "":
                out_file.write(line)
            elif float(split_line[allelefreq_index]) < float(maf_cutoff):
                out_file.write(line)
    
    in_file.close()
    out_file.close()

def check_alt_in_genotype(genotype):
    genotype = genotype.split(":")[0]
    if "." in genotype:
        return None
    elif "1" in genotype:
        return True
    else:
        return False

def check_alt_allele_status(genotype):
    genotype = genotype.split(":")[0]
    if "." in genotype:
        return None
    elif genotype.count("1") == 1:
        return "heterozygous"
    elif genotype.count("1") == 2:
        return "homozygous"

def check_inherited_variant(proband_genotype, father_genotype, mother_genotype, inherited_from = "father"):
    proband_alt = check_alt_in_genotype(proband_genotype)
    father_alt = check_alt_in_genotype(father_genotype)
    mother_alt = check_alt_in_genotype(mother_genotype)
    if check_alt_allele_status(proband_genotype) == "homozygous":
        meet_criteria = all([father_alt, mother_alt])
    else:
        if inherited_from.lower() == "father":
            meet_criteria = all([proband_alt, father_alt, not mother_alt])
        elif inherited_from.lower() == "mother":
            meet_criteria = all([proband_alt, not father_alt, mother_alt])
    return meet_criteria

def extract_inherited_variant(input, output, proband_id, father_id, mother_id, inherited_from):
    in_file = open(input, "r")
    out_file = open(output, "w")
    
    for line in in_file:
        if line.startswith("##"):
            out_file.write(line)
        elif line.startswith("#CHROM"):
            header = line.split()
            proband_index = header.index(proband_id)
            father_index = header.index(father_id)
            mother_index = header.index(mother_id)
            out_file.write(line)
        elif not line.startswith("#"):
            split_line = line.split()
            proband_genotype = split_line[proband_index]
            father_genotype = split_line[father_index]
            mother_genotype = split_line[mother_index]

            if "." not in proband_genotype:
                if check_inherited_variant(proband_genotype, father_genotype, mother_genotype, inherited_from):
                    out_file.write(line)
    out_file.close()
    in_file.close()

def check_alt_allele_ratio(genotype, allele_counts):    
    allele_counts = allele_counts.split(",")
    ref_count = float(allele_counts[0])
    alt_count = float(allele_counts[1])
    total_allele_count = ref_count + alt_count
    
    if total_allele_count == 0:
        return False
    else:
        num_alt_alleles = genotype.count("1")
        alt_allele_ratio = alt_count / (total_allele_count)
        
        if num_alt_alleles == 0:
            if alt_allele_ratio > 0.15:
                return False
            else:
                return True
        elif num_alt_alleles == 1:
            if (alt_allele_ratio < 0.3) or (alt_allele_ratio > 0.7):
                return False
            else:
                return True
        elif num_alt_alleles == 2:
            if alt_allele_ratio < 0.85:
                return False
            else:
                return True

def check_genotype_quality(quality_score):
    if int(quality_score) < 20:
        return False
    else:
        return True

def check_read_depth(read_depth):
    if int(read_depth) < 15:
        return False
    else:
        return True

def genotype_quality_control(genotype_info):
    genotype_split = genotype_info.split(":")
    if "." in genotype_split[0]:
        return None
    else:
        pass_alt_allele_ratio = check_alt_allele_ratio(genotype_split[0], genotype_split[1])
        pass_depth = check_read_depth(genotype_split[2])
        pass_genotype_quality = check_genotype_quality(genotype_split[3])
        pass_qc = all([pass_alt_allele_ratio, pass_depth, pass_genotype_quality])
        return pass_qc

def extract_denovo_variants(input, output, proband_id, father_id, mother_id):
    in_file = open(input, "r")
    out_file = open(output, "w")
    
    for line in in_file:
        if line.startswith("##"):
            out_file.write(line)
        elif line.startswith("#CHROM"):
            header = line.split()
            proband_index = header.index(proband_id)
            father_index = header.index(father_id)
            mother_index = header.index(mother_id)
            out_file.write(line)
        elif not line.startswith("#"):
            split_line = line.split()
            proband_genotype = split_line[proband_index]
            father_genotype = split_line[father_index]
            mother_genotype = split_line[mother_index]
            
            # print(proband_genotype)
            if "." not in proband_genotype:
                proband_alt = check_alt_in_genotype(proband_genotype)
                father_alt = check_alt_in_genotype(father_genotype)
                mother_alt = check_alt_in_genotype(mother_genotype)
                
                if not any([proband_alt == None, father_alt == None, mother_alt == None]):
                    # Check genotype quality
                    proband_qc = genotype_quality_control(proband_genotype)
                    father_qc = genotype_quality_control(father_genotype)
                    mother_qc = genotype_quality_control(mother_genotype)
                
                    # print(proband_genotype)
                    # print(proband_alt_allele_ratio)
                    if all([proband_alt,
                        father_alt == False,
                        mother_alt == False,
                        proband_qc,
                        father_qc,
                        mother_qc]):
                        
                        out_file.write(line)
    out_file.close()
    in_file.close()

################################################################################
# Begin Rules
################################################################################
rule all:
    input:
        "results/variant_counts/wgs_phased_all_variants.hg19_multianno.genic.lod100.tbx1_counts.txt",
        "results/variant_counts/wgs_phased_all_variants.hg19_multianno.lod100.tbx1_counts.txt",
        "results/novel_variants/novel_deleterious.snv.txt",
        "results/novel_variants/novel_deleterious.indel.txt",
        "results/rare_variants/rare_genic_deleterious.snv.txt",
        "results/rare_variants/rare_genic_deleterious.indel.txt"

rule split_indel_snv_novel:
    input: "results/novel_variants/novel_deleterious_variants.txt"
    params: out_prefix = "results/novel_variants/novel_deleterious"
    output:
        "results/novel_variants/novel_deleterious.snv.txt",
        "results/novel_variants/novel_deleterious.indel.txt"
    run:
        split_indel_and_snv(input[0], params.out_prefix)

rule split_indel_snv_rare:
    input: "results/rare_variants/rare_genic_deleterious_variants.txt"
    params: out_prefix = "results/rare_variants/rare_genic_deleterious"
    output: "results/rare_variants/rare_genic_deleterious.snv.txt",
        "results/rare_variants/rare_genic_deleterious.indel.txt"
    run:
        split_indel_and_snv(input[0], params.out_prefix)

rule count_variants_tbx1_pathway_genic_lod100:
    input:
        "results/annovar_output/genic_lod100/wgs_phased_all_variants.hg19_multianno.genic.lod100.txt",
        "data/tbx1_pathway_genes_2014_03_27.txt"
    output:
        "results/variant_counts/wgs_phased_all_variants.hg19_multianno.genic.lod100.tbx1_counts.txt"
    shell: """
    perl src/perl/count_var_per_gene.pl \
    {input[1]} \
    {input[0]} \
    {output}
    """

rule count_variants_tbx1_pathway_all_lod100:
    input:
        "results/annovar_output/all_lod100/wgs_phased_all_variants.hg19_multianno.lod100.txt",
        "data/tbx1_pathway_genes_2014_03_27.txt"
    output:
        "results/variant_counts/wgs_phased_all_variants.hg19_multianno.lod100.tbx1_counts.txt"
    shell: """
    perl src/perl/count_var_per_gene.pl \
    {input[1]} \
    {input[0]} \
    {output}
    """

################
# Extract variants
################

# rule extract_novel_variants:
#     input: "results/annovar_output/multiannotation/wgs_phased_all_variants.hg19_multianno.txt"
#     output: "results/novel_variants/novel_variants.txt"
#     run:
#         remove_previously_reported(input[0], output[0])

# rule all_lod100:
#     input: "results/annovar_output/multiannotation/wgs_phased_{VAR_TYPE}.hg19_multianno.txt"
#     output: "results/annovar_output/variant_filtering/all_lod100/wgs_phased_{VAR_TYPE}.hg19_multianno.lod100.txt"
#     run:
#         in_file = open(input[0], "r")
#         out_file = open(output[0], "w")
#         lod_index = None
        
#         for line in in_file:
#             if line.startswith("Chr\tStart"):
#                 split_line = line.split("\t")
#                 lod_index = split_line.index("phastConsElements46way")
#             else:
#                 split_line = line.split("\t")
#                 lod_column = split_line[lod_index]
                
#                 if not lod_column == "":
#                     lod_score = lod_column.split(";")[1]
#                     lod_score = lod_score.split("=")[2]
#                     if int(lod_score) >= 100:
#                         out_file.write(line)
#         in_file.close()
#         out_file.close()

# rule genic_lod100:
#     input: "results/annovar_output/variant_filtering/genic/wgs_phased_{VAR_TYPE}.hg19_multianno.genic.txt"
#     params:
#         conservation_column = "phastConsElements46way",
#         lod_cutoff = "100"
#     output: "results/annovar_output/variant_filtering/genic_lod100/wgs_phased_{VAR_TYPE}.hg19_multianno.genic.lod100.txt"
#     run:
#         extract_conservation(input[0], 
#                              output[0], 
#                              params.conservation_column, 
#                              int(params.lod_cutoff))

# rule genic:
#     input: "results/annovar_output/multiannotation/wgs_phased_{VAR_TYPE}.hg19_multianno.txt"
#     output: "results/annovar_output/variant_filtering/genic/wgs_phased_{VAR_TYPE}.hg19_multianno.genic.txt"
#     run:
#         extract_genic_variants(input[0], output[0]) 



rule denovo_novel_rare_all:
    input:
        "results/annovar_output/variant_count_summary/denovo.wgs_phased_snp.all_samples.summary.txt",
        "results/annovar_output/variant_count_summary/denovo.wgs_phased_indel.all_samples.summary.txt",
        expand("results/annovar_output/variant_filtering/novel/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.novel.genic.deleterious.counts.txt", VAR_TYPE = var_type, SAMPLEID = probandid),
        expand("results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.genic.deleterious.counts.txt", VAR_TYPE = var_type, SAMPLEID = probandid),
        expand("results/annovar_output/variant_filtering/remaining_22q11/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.remaining22q11.genic.deleterious.txt", VAR_TYPE = var_type, SAMPLEID = probandid),
        expand("results/annovar_output/variant_filtering/remaining_22q11/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.remaining22q11.summary.txt", VAR_TYPE = var_type, SAMPLEID = probandid)

################################################################################
# de novo variant extraction
################################################################################
rule denovo_all:
    input:
        "results/annovar_output/variant_count_summary/denovo.wgs_phased_snp.all_samples.summary.txt",
        "results/annovar_output/variant_count_summary/denovo.wgs_phased_indel.all_samples.summary.txt"

rule combine_variant_counts_denovo:
    input:
        expand("results/annovar_output/variant_count_summary/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.txt", VAR_TYPE = var_type, SAMPLE = ["BM1452.001", "BM1453.001"])
    output:
        "results/annovar_output/variant_count_summary/denovo.wgs_phased_snp.all_samples.summary.txt",
        "results/annovar_output/variant_count_summary/denovo.wgs_phased_indel.all_samples.summary.txt"
    shell:"""
        Rscript workflow/scripts/merge_variant_count_summaries.R \
        {output[0]} \
        {output[1]} \
        {input}
    """

rule summarize_denovo_variant_counts:
    input: 
        multianno = "results/annovar_output/multiannotation/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.hg19_multianno.txt",
        dbsnp = "results/annovar_output/multiannotation/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.hg19_snp138_dropped",
        onekg = "results/annovar_output/multiannotation/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.hg19_ALL.sites.2012_04_dropped",
        exonic = "results/annovar_output/multiannotation/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.refGene.exonic_variant_function",
        variant_function = "results/annovar_output/multiannotation/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.refGene.variant_function"
    output: 
        "results/annovar_output/variant_count_summary/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.txt"
    shell: """
        grep -v -P "Chr\tStart" {input.multianno} | grep -v -E "^#" | awk 'END {{print "total\t", NR}}' > {output}
        awk 'END {{print "In dbSNP138\t", NR}}' {input.dbsnp} >> {output}
        awk 'END {{print "In 1kg (2012/10)\t", NR}}' {input.onekg} >> {output}
        awk 'END {{print "Exonic\t", NR}}' {input.exonic} >> {output}
        grep "nonsynonymous" {input.exonic} | awk 'END {{print "Nonsynonymous\t", NR}}' >> {output}
        grep "stop" {input.multianno} | awk 'END {{print "Stop gain/loss\t", NR}}' >> {output}
        grep "splicing" {input.multianno} | awk 'END {{print "Splice altering\t", NR}}' >> {output}
        grep -w "frameshift" {input.multianno} | awk 'END {{print "Frameshift\t", NR}}' >> {output}
        grep -w "intronic" {input.variant_function} | awk 'END {{print "Intronic\t", NR}}' >> {output}
        grep -w "upstream" {input.variant_function} | awk 'END {{print "Upstream\t", NR}}' >> {output}
        grep -w "downstream" {input.variant_function} | awk 'END {{print "Downstream\t", NR}}' >> {output}
        grep -w "intergenic" {input.variant_function} | awk 'END {{print "Intergenic\t", NR}}' >> {output}
    """

rule table_annotation_denovo:
    input: 
        "data/for_annovar/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.recode.avinput"
    params: 
        output_prefix = "results/annovar_output/multiannotation/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo"
    output: 
        "results/annovar_output/multiannotation/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.hg19_multianno.txt",
        "results/annovar_output/multiannotation/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.hg19_snp138_dropped",
        "results/annovar_output/multiannotation/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.hg19_ALL.sites.2012_04_dropped",
        "results/annovar_output/multiannotation/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.refGene.exonic_variant_function",
        "results/annovar_output/multiannotation/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.refGene.variant_function"
    shell: """
    {annovar_dir}/table_annovar.pl \
    {input} \
    {annovar_dir}/humandb/ \
    --protocol refGene,phastConsElements46way,genomicSuperDups,esp6500si_all,1000g2012apr_all,snp138,ljb23_all \
    --operation g,r,r,f,f,f,f \
    --buildver hg19 \
    --outfile {params.output_prefix} \
    --otherinfo
    """

rule convert_denovo_to_avinput:
    input:
        "data/indiv_samples/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.recode.vcf"
    output:
        "data/for_annovar/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.recode.avinput"
    shell: """
        {annovar_dir}/convert2annovar.pl \
        -format vcf4 {input} \
        -outfile {output} \
        -includeinfo \
        -filter PASS \
        -comment
    """

rule split_denovo_vcf_file:
    input: "data/indiv_samples/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.vcf"
    params: 
        subject = "{SAMPLE}",
        output_prefix = "data/indiv_samples/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo"
    output: "data/indiv_samples/denovo/wgs_phased_{VAR_TYPE}.sample.{SAMPLE}.denovo.recode.vcf"
    shell: """
        {vcftools} \
        --vcf {input} \
        --indv {params.subject} \
        --out {params.output_prefix} \
        --recode \
        --recode-INFO-all
    """

rule extract_denovo_variants_bm1453:
    input:
        "data/wgs_phased_{VAR_TYPE}.vcf"
    params:
        proband = "BM1453.001",
        father = "BM1453.200",
        mother = "BM1453.100"
    output:
        "data/indiv_samples/denovo/wgs_phased_{VAR_TYPE}.sample.BM1453.001.denovo.vcf"
    run:
        extract_denovo_variants(input[0], 
                                output[0], 
                                params.proband, 
                                params.father, 
                                params.mother)

rule extract_denovo_variants_bm1452:
    input:
        "data/wgs_phased_{VAR_TYPE}.vcf"
    params:
        proband = "BM1452.001",
        father = "BM1452.200",
        mother = "BM1452.100"
    output:
        "data/indiv_samples/denovo/wgs_phased_{VAR_TYPE}.sample.BM1452.001.denovo.vcf"
    run:
        extract_denovo_variants(input[0], 
                                output[0], 
                                params.proband, 
                                params.father, 
                                params.mother)

################################################################################
# Rare inherited extraction
################################################################################
rule rare_inherited_all:
    input: expand("results/annovar_output/variant_filtering/rare_inherited/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.genic.deleterious.txt", VAR_TYPE = var_type, SAMPLEID = "BM1452.001")

rule rare_inherited_extract_deleterious:
    input:
        "results/annovar_output/variant_filtering/rare_inherited/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.genic.txt"
    params: deleterious_score_cutoff = "2"
    output:
        "results/annovar_output/variant_filtering/rare_inherited/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.genic.deleterious.txt"
    run:
        extract_deleterious_variants(input[0], output[0], params.deleterious_score_cutoff)

rule rare_inherited_extract_genic:
    input: "results/annovar_output/variant_filtering/rare_inherited/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.txt"
    output: "results/annovar_output/variant_filtering/rare_inherited/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.genic.txt"
    run:
        extract_genic_variants(input[0], output[0])

rule rare_inherited_extract_population:
    input:
        "results/annovar_output/variant_filtering/rare_inherited/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.txt"
    params: maf_cutoff = "0.05"
    output:
        "results/annovar_output/variant_filtering/rare_inherited/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.txt"
    run:
        extract_rare_variants(input[0], output[0], params.maf_cutoff)

rule rare_inherited_remove_superdup:
    input: "results/annovar_output/inherited_multiannotation/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_multianno.txt"
    output: "results/annovar_output/variant_filtering/rare_inherited/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.txt"
    run:
        remove_superdup(input[0], output[0])

rule rare_inherited_table_annotation_indiv:
    input: "results/indiv_avinput/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.avinput"
    params: output_prefix = "results/annovar_output/inherited_multiannotation/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}"
    output: 
        "results/annovar_output/inherited_multiannotation/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_multianno.txt",
        "results/annovar_output/inherited_multiannotation/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_snp138_dropped",
        "results/annovar_output/inherited_multiannotation/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_ALL.sites.2012_04_dropped",
        "results/annovar_output/inherited_multiannotation/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.refGene.exonic_variant_function"
    shell: """
    {annovar_dir}/table_annovar.pl \
    {input} \
    {annovar_dir}/humandb/ \
    --protocol refGene,phastConsElements46way,genomicSuperDups,esp6500si_all,1000g2012apr_all,snp138,ljb23_all \
    --operation g,r,r,f,f,f,f \
    --buildver hg19 \
    --outfile {params.output_prefix} \
    --otherinfo
    """

rule rare_inherited_convert_to_annovar_indiv:
    input: "data/indiv_samples/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.recode.vcf"
    output: "results/indiv_avinput/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.avinput"
    shell: """
        {annovar_dir}/convert2annovar.pl \
        -format vcf4 {input} \
        -outfile {output} \
        -includeinfo \
        -filter PASS \
        -comment
    """

rule rare_inherited_qc_genotype_indiv:
    input: "data/indiv_samples/inherited.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.recode.vcf"
    params: sample = "{SAMPLEID}"
    output: "data/indiv_samples/inherited.qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.recode.vcf"
    run:
        in_file = open(input[0], "r")
        out_file = open(output[0], "w")

        for line in in_file:
            if line.startswith("##"):
                out_file.write(line)
            elif line.startswith("#CHROM"):
                line_split = line.split()
                genotype_index = line_split.index(params.sample)
                out_file.write(line)
            else:
                line_split = line.split()
                genotype = line_split[genotype_index]
                pass_qc = genotype_quality_control(genotype)

                if pass_qc:
                    out_file.write(line)

rule rare_inherited_split_vcf_file:
    input: "data/for_annovar/wgs_phased_{VAR_TYPE}_filtered_inherited.vcf"
    params: 
        subject = "{SAMPLEID}",
        output_prefix = "data/indiv_samples/inherited.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}"
    output: "data/indiv_samples/inherited.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.recode.vcf"
    shell: """
        {vcftools} \
        --vcf {input} \
        --indv {params.subject} \
        --out {params.output_prefix} \
        --recode \
        --recode-INFO-all
    """

rule rare_inherited_extract_inherited:
    input: "data/for_annovar/wgs_phased_{VAR_TYPE}_filtered.vcf"
    params:
        proband_id = "BM1452.001",
        father_id = "BM1452.200",
        mother_id = "BM1452.100",
        inherited_from = "father"
    output: "data/for_annovar/wgs_phased_{VAR_TYPE}_filtered_inherited.vcf"
    run:
        extract_inherited_variant(input[0], output[0], params.proband_id, params.father_id, params.mother_id, params.inherited_from)

################################################################################
# Rare variant extraction
################################################################################
rule process_novel_and_rare_variants:
    input:
        expand("results/annovar_output/variant_filtering/novel/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.novel.genic.deleterious.counts.txt", VAR_TYPE = var_type, SAMPLEID = probandid),
        expand("results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.genic.deleterious.counts.txt", VAR_TYPE = var_type, SAMPLEID = probandid)

rule process_rare_variants:
    input:
        expand("results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.genic.deleterious.txt", VAR_TYPE = var_type, SAMPLEID = probandid),
        expand("results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.conserved.txt", VAR_TYPE = var_type, SAMPLEID = probandid)

rule rare_summarize_variant_filtering:
    input: 
        "results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.genic.deleterious.txt"
    output: 
        "results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.genic.deleterious.counts.txt"
    shell: """
        grep -v -P "Chr\tStart" {input} | grep -v -E "^#" | awk 'END {{print "total\t", NR}}' > {output}
        grep -w "exonic" {input} | awk 'END {{print "Exonic\t", NR}}' >> {output}
        grep -w "nonsynonymous" {input} | awk 'END {{print "Nonsynonymous\t", NR}}' >> {output}
        grep -w "synonymous" {input} | awk 'END {{print "Synonymous\t", NR}}' >> {output}
        grep -w "splicing" {input} | awk 'END {{print "Splicing\t", NR}}' >>{output}
        grep "stop" {input} | awk 'END {{print "Stop gain/loss\t", NR}}' >> {output}
        grep -w "frameshift" {input} | awk 'END {{print "Frameshift\t", NR}}' >> {output}
    """

rule rare_extract_deleterious:
    input:
        "results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.genic.txt"
    params: deleterious_score_cutoff = "2"
    output:
        "results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.genic.deleterious.txt"
    run:
        extract_deleterious_variants(input[0], output[0], params.deleterious_score_cutoff)

rule rare_extract_genic:
    input: "results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.txt"
    output: "results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.genic.txt"
    run:
        extract_genic_variants(input[0], output[0])

rule rare_extract_conserved:
    input: "results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.txt"
    output: "results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.conserved.txt"
    params:
        conservation_column = "phastConsElements46way",
        score_cutoff = "100"
    run:
        extract_conserved_elements(input[0], output[0], params.conservation_column, params.score_cutoff)

rule rare_extract_population:
    input:
        "results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.txt"
    params: maf_cutoff = "0.05"
    output:
        "results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.rare.txt"
    run:
        extract_rare_variants(input[0], output[0], params.maf_cutoff)

rule rare_remove_superdup:
    input: "results/annovar_output/indiv_multiannotation/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_multianno.txt"
    output: "results/annovar_output/variant_filtering/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.txt"
    run:
        remove_superdup(input[0], output[0])
    
rule rare_table_annotation_indiv:
    input: "results/indiv_avinput/qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.avinput"
    params: output_prefix = "results/annovar_output/indiv_multiannotation/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}"
    output: 
        "results/annovar_output/indiv_multiannotation/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_multianno.txt",
        "results/annovar_output/indiv_multiannotation/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_snp138_dropped",
        "results/annovar_output/indiv_multiannotation/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_ALL.sites.2012_04_dropped",
        "results/annovar_output/indiv_multiannotation/rare/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.refGene.exonic_variant_function"
    shell: """
    {annovar_dir}/table_annovar.pl \
    {input} \
    {annovar_dir}/humandb/ \
    --protocol refGene,phastConsElements46way,genomicSuperDups,esp6500si_all,1000g2012apr_all,snp138,ljb23_all \
    --operation g,r,r,f,f,f,f \
    --buildver hg19 \
    --outfile {params.output_prefix} \
    --otherinfo
    """

################################################################################
# Remaining allele of chr22q11
################################################################################
rule check_22q11_genes:
    input:
        expand("results/annovar_output/variant_filtering/remaining_22q11/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.remaining22q11.genic.deleterious.txt", VAR_TYPE = var_type, SAMPLEID = probandid),
        expand("results/annovar_output/variant_filtering/remaining_22q11/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.remaining22q11.summary.txt", VAR_TYPE = var_type, SAMPLEID = probandid)

rule summarize_chr22q11_variant_counts:
    input: "results/annovar_output/variant_filtering/remaining_22q11/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.remaining22q11.txt"
    output: "results/annovar_output/variant_filtering/remaining_22q11/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.remaining22q11.summary.txt"
    shell: """
        # total variants
        grep -v -P "Chr\tStart" {input} | grep -v -E "^#" | awk 'END {{print "Total\t", NR}}' > {output}
        grep -w "nonsynonymous" {input} | awk 'END {{print "Nonsynonymous\t", NR}}' >> {output}
        grep -w "synonymous" {input} | awk 'END {{print "Synonymous\t", NR}}' >> {output}
        egrep -w "UTR3|UTR5" {input} | awk 'END {{print "UTR\t", NR}}' >> {output}
        grep -w "intronic" {input} | awk 'END {{print "Intronic\t", NR}}' >> {output}
        grep -w "upstream" {input} | awk 'END {{print "Upstream\t", NR}}' >> {output}
        grep -w "downstream" {input} | awk 'END {{print "Downstream\t", NR}}' >> {output}
        grep -w "intergenic" {input} | awk 'END {{print "Intergenic\t", NR}}' >> {output}
        egrep -w "ncRNA_exonic|ncRNA_intronic" {input} | awk 'END {{print "ncRNA\t", NR}}' >> {output}
        grep -w "frameshift" {input} | awk 'END {{print "Frameshift\t", NR}}' >> {output}
    """

rule chr22q11_extract_deleterious:
    input: "results/annovar_output/variant_filtering/remaining_22q11/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.remaining22q11.genic.nonsyn.txt"
    params: deleterious_cutoff = "2"
    output: "results/annovar_output/variant_filtering/remaining_22q11/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.remaining22q11.genic.deleterious.txt"
    run:
        extract_deleterious_variants(input[0], output[0], params.deleterious_cutoff)

rule chr22q11_extract_nonsynonymous:
    input: "results/annovar_output/variant_filtering/remaining_22q11/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.remaining22q11.genic.txt"
    params: deleterious_cutoff = "0"
    output: "results/annovar_output/variant_filtering/remaining_22q11/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.remaining22q11.genic.nonsyn.txt"
    run:
        extract_deleterious_variants(input[0], output[0], params.deleterious_cutoff)

rule chr22q11_extract_genic:
    input:
        "results/annovar_output/variant_filtering/remaining_22q11/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.remaining22q11.txt"
    output:
        "results/annovar_output/variant_filtering/remaining_22q11/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.remaining22q11.genic.txt"
    run:
        extract_genic_variants(input[0], output[0])

rule extract_22q11_genes:
    input:
        "results/annovar_output/indiv_multiannotation/novel/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_multianno.txt"
    params:
        chromosome = "chr22",
        start = "18656000",
        stop = "21792000"
    output: "results/annovar_output/variant_filtering/remaining_22q11/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.remaining22q11.txt"
    run:
        variants_file = open(input[0], "r")
        out_file = open(output[0], "w")
        
        for line in variants_file:
            if line.startswith("Chr\tStart"):
                out_file.write(line)
            elif line.startswith("#"):
                out_file.write(line)
            else:
                line_split = line.split()
                if all([line_split[0] == params.chromosome, int(line_split[1]) >= int(params.start), int(line_split[2]) <= int(params.stop)]):
                    out_file.write(line)

        variants_file.close()
        out_file.close()

################################################################################
# Novel variant extraction
################################################################################
rule process_novel_variants:
    input:
        expand("results/annovar_output/variant_filtering/novel/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.novel.genic.deleterious.txt", VAR_TYPE = var_type, SAMPLEID = probandid)

rule novel_summarize_variant_filtering:
    input: 
        "results/annovar_output/variant_filtering/novel/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.novel.genic.deleterious.txt"
    output: 
        "results/annovar_output/variant_filtering/novel/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.novel.genic.deleterious.counts.txt"
    shell: """
        grep -v -P "Chr\tStart" {input} | grep -v -E "^#" | awk 'END {{print "total\t", NR}}' > {output}
        grep -w "exonic" {input} | awk 'END {{print "Exonic\t", NR}}' >> {output}
        grep -w "nonsynonymous" {input} | awk 'END {{print "Nonsynonymous\t", NR}}' >> {output}
        grep -w "synonymous" {input} | awk 'END {{print "Synonymous\t", NR}}' >> {output}
        grep -w "splicing" {input} | awk 'END {{print "Splicing\t", NR}}' >>{output}
        grep "stop" {input} | awk 'END {{print "Stop gain/loss\t", NR}}' >> {output}
        grep -w "frameshift" {input} | awk 'END {{print "Frameshift\t", NR}}' >> {output}
    """

rule novel_extract_deleterious:
    input:
        "results/annovar_output/variant_filtering/novel/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.novel.genic.txt"
    params: deleterious_score_cutoff = "2"
    output:
        "results/annovar_output/variant_filtering/novel/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.novel.genic.deleterious.txt"
    run:
        extract_deleterious_variants(input[0], output[0], params.deleterious_score_cutoff)

rule novel_extract_genic:
    input: "results/annovar_output/variant_filtering/novel/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.novel.txt"
    output: "results/annovar_output/variant_filtering/novel/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.novel.genic.txt"
    run:
        extract_genic_variants(input[0], output[0])

rule novel_extract_population:
    input:
        "results/annovar_output/variant_filtering/novel/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.txt"
    params: dbsnp_version = "snp138"
    output:
        "results/annovar_output/variant_filtering/novel/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.novel.txt"
    run:
        remove_previously_reported(input[0], output[0], params.dbsnp_version)

rule novel_remove_superdup:
    input: "results/annovar_output/indiv_multiannotation/novel/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_multianno.txt"
    output: "results/annovar_output/variant_filtering/novel/snp138.wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.nosuperdup.txt"
    run:
        remove_superdup(input[0], output[0])

################################################################################
# Variants for individual subjects
################################################################################

rule combine_variant_counts:
    input:
        expand("results/annovar_output/variant_count_summary/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.summary.txt", VAR_TYPE = var_type, SAMPLEID = sampleid),
        expand("results/annovar_output/variant_count_summary/wgs_phased_{VAR_TYPE}.summary.txt", VAR_TYPE = var_type)
    output:
        "results/annovar_output/variant_count_summary/wgs_phased_snp.all_samples.summary.txt",
        "results/annovar_output/variant_count_summary/wgs_phased_indel.all_samples.summary.txt"
    shell:"""
        Rscript workflow/scripts/merge_variant_count_summaries.R \
        {output[0]} \
        {output[1]} \
        {input}
    """

rule summarize_indiv_variant_counts:
    input: 
        multianno = "results/annovar_output/indiv_multiannotation/novel/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_multianno.txt",
        dbsnp = "results/annovar_output/indiv_multiannotation/novel/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_snp138_dropped",
        onekg = "results/annovar_output/indiv_multiannotation/novel/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_ALL.sites.2012_04_dropped",
        exonic = "results/annovar_output/indiv_multiannotation/novel/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.refGene.exonic_variant_function"
    output: "results/annovar_output/variant_count_summary/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.summary.txt"   # update output path
    shell: """
        # total variants
        grep -v -P "Chr\tStart" {input.multianno} | grep -v -E "^#" | awk 'END {{print "total\t", NR}}' > {output}
        awk 'END {{print "In dbSNP138\t", NR}}' {input.dbsnp} >> {output}
        awk 'END {{print "In 1kg (2012/10)\t", NR}}' {input.onekg} >> {output}
        awk 'END {{print "Exonic\t", NR}}' {input.exonic} >> {output}
        grep "nonsynonymous" {input.exonic} | awk 'END {{print "Nonsynonymous\t", NR}}' >> {output}
        grep "stop" {input.multianno} | awk 'END {{print "Stop gain/loss\t", NR}}' >> {output}
        grep "splicing" {input.multianno} | awk 'END {{print "Splice altering\t", NR}}' >> {output}
        grep -w "frameshift" {input.multianno} | awk 'END {{print "Frameshift\t", NR}}' >> {output}
    """

rule novel_table_annotation_indiv:
    input: "results/indiv_avinput/qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.avinput"
    params: output_prefix = "results/annovar_output/indiv_multiannotation/novel/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}"
    output: 
        "results/annovar_output/indiv_multiannotation/novel/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_multianno.txt",
        "results/annovar_output/indiv_multiannotation/novel/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_snp138_dropped",
        "results/annovar_output/indiv_multiannotation/novel/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.hg19_ALL.sites.2012_04_dropped",
        "results/annovar_output/indiv_multiannotation/novel/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.refGene.exonic_variant_function"
    shell: """
    {annovar_dir}/table_annovar.pl \
    {input} \
    {annovar_dir}/humandb/ \
    --protocol refGene,phastConsElements46way,genomicSuperDups,esp6500si_all,1000g2012apr_all,snp138,ljb23_all \
    --operation g,r,r,f,f,f,f \
    --buildver hg19 \
    --outfile {params.output_prefix} \
    --otherinfo
    """

################################################################################
# All sample CADD table annotations
################################################################################
rule filter_cadd:
    input: "results/annovar_output/cadd_annotation/wgs_phased_snp.cadd.hg19_cadd_dropped"
    params: scaled_cadd_cutoff = "20"
    output: "results/annovar_output/cadd_annotation/wgs_phased_snp.cadd.hg19_cadd_dropped.filtered"
    run:
        in_file = open(input[0], "r")
        out_file = open(output[0], "w")

        for line in in_file:
            split_line = line.split("\t")
            cadd_score = split_line[1].split(",")
            scaled_cadd = cadd_score[1]
            if float(scaled_cadd) >= int(params.scaled_cadd_cutoff):
                out_file.write(line)
        in_file.close()
        out_file.close()

rule annotate_novel_cadd:
    input: "results/indiv_avinput/qc_wgs_phased_snp.sample.BM1452.001.avinput"
    params: output_prefix = "results/annovar_output/cadd_annotation/wgs_phased_snp.cadd"
    output: "results/annovar_output/cadd_annotation/wgs_phased_snp.cadd.hg19_cadd_dropped"
    shell: """
        {annovar_dir}/annotate_variation.pl \
            {input} \
            {annovar_dir}/humandb/ \
            -filter \
            -dbtype cadd \
            -buildver hg19 \
            -out {params.output_prefix} \
            -otherinfo
    """

################################################################################
# Prepare vcf files for individual samples
################################################################################
rule convert_to_annovar_indiv:
    input: "data/indiv_samples/qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.recode.vcf"
    output: "results/indiv_avinput/qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.avinput"
    shell: """
        {annovar_dir}/convert2annovar.pl \
        -format vcf4 {input} \
        -outfile {output} \
        -includeinfo \
        -filter PASS \
        -comment
    """

rule qc_genotype_indiv:
    input: "data/indiv_samples/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.recode.vcf"
    params: sample = "{SAMPLEID}"
    output: "data/indiv_samples/qc_wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.recode.vcf"
    run:
        in_file = open(input[0], "r")
        out_file = open(output[0], "w")

        for line in in_file:
            if line.startswith("##"):
                out_file.write(line)
            elif line.startswith("#CHROM"):
                line_split = line.split()
                genotype_index = line_split.index(params.sample)
                out_file.write(line)
            else:
                line_split = line.split()
                genotype = line_split[genotype_index]
                pass_qc = genotype_quality_control(genotype)

                if pass_qc:
                    out_file.write(line)

rule split_vcf_file:
    input: "data/wgs_phased_{VAR_TYPE}.vcf"
    params: 
        subject = "{SAMPLEID}",
        output_prefix = "data/indiv_samples/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}"
    output: "data/indiv_samples/wgs_phased_{VAR_TYPE}.sample.{SAMPLEID}.recode.vcf"
    shell: """
        {vcftools} \
        --vcf {input} \
        --indv {params.subject} \
        --out {params.output_prefix} \
        --recode \
        --recode-INFO-all
    """

################################################################################
# All sample table annotations
################################################################################
rule test:
    input: expand("results/annovar_output/variant_count_summary/wgs_phased_{VAR_TYPE}.summary.txt", VAR_TYPE = var_type)

rule summarize_variant_counts:
    input: 
        multianno = "results/annovar_output/multiannotation/wgs_phased_{VAR_TYPE}.hg19_multianno.txt",
        dbsnp = "results/annovar_output/multiannotation/wgs_phased_{VAR_TYPE}.hg19_snp138_dropped",
        onekg = "results/annovar_output/multiannotation/wgs_phased_{VAR_TYPE}.hg19_ALL.sites.2012_04_dropped",
        exonic = "results/annovar_output/multiannotation/wgs_phased_{VAR_TYPE}.refGene.exonic_variant_function"
    output: "results/annovar_output/variant_count_summary/wgs_phased_{VAR_TYPE}.summary.txt"
    shell: """
        grep -v -P "Chr\tStart" {input.multianno} | grep -v -E "^#" | awk 'END {{print "total\t", NR}}' > {output}
        awk 'END {{print "In dbSNP138\t", NR}}' {input.dbsnp} >> {output}
        awk 'END {{print "In 1kg (2012/10)\t", NR}}' {input.onekg} >> {output}
        awk 'END {{print "Exonic\t", NR}}' {input.exonic} >> {output}
        grep "nonsynonymous" {input.exonic} | awk 'END {{print "Nonsynonymous\t", NR}}' >> {output}
        grep "stop" {input.multianno} | awk 'END {{print "Stop gain/loss\t", NR}}' >> {output}
        grep "splicing" {input.multianno} | awk 'END {{print "Splice altering\t", NR}}' >> {output}
        grep -w "frameshift" {input.multianno} | awk 'END {{print "Frameshift\t", NR}}' >> {output}
    """

rule table_annotation_novel:
    input: "data/for_annovar/wgs_phased_{VAR_TYPE}.avinput"
    params: output_prefix = "results/annovar_output/multiannotation/wgs_phased_{VAR_TYPE}"
    output:
        "results/annovar_output/multiannotation/wgs_phased_{VAR_TYPE}.hg19_multianno.txt",
        "results/annovar_output/multiannotation/wgs_phased_{VAR_TYPE}.hg19_snp138_dropped",
        "results/annovar_output/multiannotation/wgs_phased_{VAR_TYPE}.hg19_ALL.sites.2012_04_dropped",
        "results/annovar_output/multiannotation/wgs_phased_{VAR_TYPE}.refGene.exonic_variant_function"
    output: "results/annovar_output/variant_count_summary/wgs_phased_{VAR_TYPE}.summary.txt"
    shell: """
    {annovar_dir}/table_annovar.pl \
    {input} \
    {annovar_dir}/humandb/ \
    --protocol refGene,phastConsElements46way,genomicSuperDups,esp6500si_all,1000g2012apr_all,snp138,ljb23_all \
    --operation g,r,r,f,f,f,f \
    --buildver hg19 \
    --outfile {params.output_prefix} \
    --otherinfo
    """

rule convert_allsample_to_avinput:
    input: "data/for_annovar/wgs_phased_{VAR_TYPE}_filtered.vcf"
    output: "data/for_annovar/wgs_phased_{VAR_TYPE}.avinput"
    message: "Converting {input} into format for annovar. Use '--format vcf4old` to keep all variants"
    shell:"""
        {annovar_dir}/convert2annovar.pl \
        -format vcf4old {input} \
        -outfile {output} \
        -includeinfo \
        -filter PASS \
        -allallele \
        -comment
    """

rule filter_vcf_file:
    input: "data/wgs_phased_{VAR_TYPE}.vcf"
    output: "data/for_annovar/wgs_phased_{VAR_TYPE}_filtered.vcf"
    run:
        in_file = open(input[0], "r")
        out_file = open(output[0], "w")

        for line in in_file:
            if line.startswith("##"):
                out_file.write(line)
            elif line.startswith("#CHROM\tPOS"):
                header = line.split()
                genotype_start_index = header.index("FORMAT") + 1
                genotype_end_index = len(header)
                out_file.write(line)
            else:
                line_split = line.split()
                test_genotype = []
                for genotype_index in range(genotype_start_index, genotype_end_index):
                    check_genotype = genotype_quality_control(line_split[genotype_index])
                    check_alt_allele = check_alt_in_genotype(line_split[genotype_index])
                    test_genotype.append(all([check_genotype,check_alt_allele]))
                if any(test_genotype):
                    out_file.write(line)
        in_file.close()
        out_file.close()
